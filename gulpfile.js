var gulp = require('gulp');
var gutil = require('gulp-util');
var fileinclude = require('gulp-file-include');
var paths = {
  src: '/wamp/www/ostarland/src/', // Modify according to your environment
  pages: ['src/*', 'src/**/*']
};

gulp.task('fileinclude', function() {

  gulp.src(['src/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: paths.src
    })
    .on('error', gutil.log))
    .pipe(gulp.dest('./'));

});

// Watch, run the compiler when a file changes
gulp.task('watch', function() {
  gulp.watch(paths.pages, ['fileinclude'])
});

// Default task, recompile on startup.
gulp.task('default', ['fileinclude','watch']);

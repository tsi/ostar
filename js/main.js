
$(document).ready(function(){

  // Development file watcher - REMOVE FROM PRODUCTION.
  var isWebkit = 'WebkitAppearance' in document.documentElement.style
  if (location.host == 'ostar.lh' && isWebkit) {
    $(document).watch('/js/main.js');
    $(document).watch('/css/style.css');
    $(document).watch('/index.html');
  }

  // check JS support
  $('html').removeClass('no-js');

});


var Intro = (function() {
  // Dynamic intro height.
  // var pushHeight = $(window).height() - $('.join').outerHeight(),
  //     headerHeight = $('.header').outerHeight(),
  //     introHeight = pushHeight - headerHeight;
  // $('.intro .int-wrp').css({
  //   'height': introHeight,
  //   'width': introHeight
  // });

  $(window).scroll(function() {
    if($(document).scrollTop() > 500 ) {
      $('.join.ext-wrp').slideDown();
    } else {
      $('.join.ext-wrp').slideUp();
    }
  })

  // Animate Scroll to Anchor
  $('.read-more').find('a').click(function(e){
    e.preventDefault();
    $('html,body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
  })

  // $('.intro').css({
  //   'margin-top': headerHeight
  // });
  // $('.vision').css({
  //   'margin-top': pushHeight
  // });
}());


var People = (function() {

  // Open person big frame.
  var openFrame = function(that, callback) {

    var that = $('.person.active', '.people'),
        slidePX,
        half,
        frame;

    if (that) {
      slidePX = that.height() * 2;

      if (that.is('.first-half')) {
        half = '.first-half';
        frame = '.people-frame-first';
      } else {
        half = '.second-half';
        frame = '.people-frame-second';
      };

      var frameBg = that.find('img').attr('src')
        .replace("png", "jpg")
        .replace("small", "842X804");
      $(frame)
        .css('background-image', 'url(' + frameBg + ')')
        .html($('.person-frame', that).html())
        .addClass('active');

      $('.person.slide-up' + half).animate({
        top: '-' + slidePX
      }, 500);
      $('.person.slide-down' + half).animate({
        top: slidePX
      }, 500);

    }
  }

  // Close person big frame.
  var closeFrame = function(reopen) {

    $('.people-frame').removeClass('active');

    $('.person').each(function() {
        $(this).animate({
          top: 0
        }, 600);
    });

    if (reopen) {
      $('.person').promise().done(openFrame);
    }
    else {
      $('.person').removeClass('active');
    }
  }

  $('.person').on({
    click: function() {
      if ($(this).is('.focused') || $(this).is(':hover')) {

        if ($(this).is('.active')) {
          $('.person', '.people').removeClass('active');
          closeFrame(false);
          return;
        }

        $('.person', '.people')
          .removeClass('active')
          .filter(this)
          .addClass('active');

        if ($('.people-frame.active').length) {
          closeFrame(true);
        }
        else {
          $(this).addClass('active');
          openFrame();
        }
      }
      else {
        $('.person', '.people')
          .removeClass('focused')
          .filter(this)
          .addClass('focused');
        closeFrame(false);
      }
    }
  });

  $('.people').on('click', '.close', function(e) {
    closeFrame(false);
  });

}());


var Tabs = (function() {

  // Tabs
  $('.tabs').on('click', '.tab', function(e) {
    var i = $(this).index(),
        tab = $(this),
        tabContent = $('.tab-content', '.tab-contents').eq(i);

    if (tab.is('.active')) {
      return;
    }

    tab.siblings('.triangle').stop().animate({
      right: i * 25 + '%'
    }, 400);

    tab.siblings('.active').removeClass('active');
    tab.addClass('active');

    tabContent.siblings('.active').slideUp().removeClass('active');
    tabContent.slideDown().addClass('active');
  });

}());


var Videos = (function() {

  $.fn.loadVideo = function(options) {
    var defaults = {
          videoID: $(this).attr('data-video-id'),
          targetID: 'vidplayer',
          height: '315',
          width: '560'
        },
        opt = $.extend({}, defaults, options);

    $('iframe#' + opt.targetID).attr('src', 'http://www.youtube.com/embed/' + opt.videoID + '?autohide=1&autoplay=0&showinfo=0&wmode=transparent&theme=light');
  }

  $(document).ready(function() {
    $('[data-video-id]', '#video-gallery').on('click', function(e) {
      e.preventDefault();
      $(this).loadVideo();
    });
  });

}());


var Lightbox =  (function(){

  $(document).on('click', ".lgt-box[data-video-id]", function(e) {

    e.preventDefault();

    // Create Lightbox Elements on video click;
    var lbox = {
      bg : $("<div>", {'class': "lbox-bg"}),
      wrp : $("<div>", {'class': "lbox-wrp"}),
      close : $("<div>", {'class': "lbox-close"}),
      contain : $("<div>", {'class': "lbox-contain"}),
      fluidWrp : $("<div>", {'class': "lbox-fluid fluid-video"}),
      iframe : $("<iframe>", {'id': "lbox-vidplayer"}),
    };

    $(lbox.iframe).appendTo(lbox.fluidWrp);
    $(lbox.fluidWrp).appendTo(lbox.contain);
    $(lbox.contain)
      .append(lbox.close)
      .appendTo(lbox.wrp);

    $(this)
      .closest('.lbox-container')
      .append(lbox.bg)
      .append(lbox.wrp);

    $(this).loadVideo({targetID: "lbox-vidplayer"});

    setTimeout(function () {
      $('.lbox-bg').addClass('lbox-active');
    }, 100);

    $('#lbox-vidplayer').load(function() {
      $(".lbox-wrp").addClass('lbox-active');
    });

    // Remove Lightbox on Close-Btn click
    $(".lbox-wrp").on("click",function(e) {
      if(!$(e.target).is('iframe')){
        e.preventDefault();
        $('.lbox-bg, .lbox-wrp').removeClass('lbox-active')
        setTimeout(function () {
          $('.lbox-bg, .lbox-wrp').remove();
        }, 400);
      }
    });

  });

}());

var LightboxGallery = (function(){

  $('.linkToGallery').on('click',function(e){
    e.preventDefault();
    // Create Lightbox Elements on video click;
    var lbox = {
      bg : $("<div>", {'class': "lbox-bg"}),
      wrp : $("<div>", {'class': "lbox-wrp lbox-gallery-wrp"}),
      close : $("<div>", {'class': "lbox-close"}),
      contain : $("<div>", {'class': "lbox-contain"}),
      // fluidWrp : $("<div>", {'class': "lbox-fluid fluid-video"}),
      gallery : $("<div>", {'id': "lbox-gallery"}),
      previewContainer: $("<div>", {'id': "lbox-gallery-preview-container"})
    };

    $(lbox.gallery).appendTo(lbox.contain);
    $(lbox.previewContainer).appendTo(lbox.contain);
    $(lbox.contain)
      .append(lbox.close)
      .appendTo(lbox.wrp);

    $(this)
      .closest('.lbox-container')
      .append(lbox.bg)
      .append(lbox.wrp);

    loadImages();

    setTimeout(function () {
      $('.lbox-bg').addClass('lbox-active');
    }, 100);

    // setTimeout(function () {
      initImageSlider();
    // }, 2000);

    // $('img','.lbox-contain').load(function(){
    //   initImageSlider();
    // });

    // Remove Lightbox on Close-Btn click
    $(".lbox-wrp").on("click",function(e) {
      var trgt = $(e.target).closest('.slick-slider')
      if(! ( trgt.is('#lbox-gallery') || trgt.is("#lbox-gallery-preview-container") )){
        e.preventDefault();
        $('.lbox-bg, .lbox-wrp').removeClass('lbox-active');
        setTimeout(function () {
          $('.lbox-bg, .lbox-wrp').remove();
        }, 400);
      }
    });
  });

  var loadImages = function(){
    var imgDir = "./images/gallery/";
    var imageArr = new Array();
    var numOfImages = 77;

    for(var i=1; i <= numOfImages; i++) {
      imageArr.push(imgDir + 'image (' + i + ').jpg');
    }

    imageArr.forEach(function(img, idx){
      $('<div><img src="' + img + '"></img></div>').prependTo('#lbox-gallery');
      $('<div><img src="' + img + '"></img></div>').prependTo('#lbox-gallery-preview-container');
    });
  }

  var initImageSlider = function(){

    // var afterChange = function(){
    //   $('.slick-slide','#lbox-gallery-preview-container').css({'width' : 'auto'});
    // }
    $('#lbox-gallery').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      // asNavFor: '#lbox-gallery-preview-container',
      variableWidth: true
    });

    $('#lbox-gallery-preview-container').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '#lbox-gallery',
      dots: false,
      centerMode: true,
      variableWidth: true,
      adaptiveHeight: true,
      focusOnSelect: true,
      onInit: function() {
        var images = $('.lbox-gallery-wrp img'),
            loaded_images_count = 0;

        images.load(function(){
          loaded_images_count++;
          if (loaded_images_count == images.length) {
            $('.lbox-wrp').addClass('lbox-active');
          }
        });
      }
    });

    // $('.slick-slide','#lbox-gallery-preview-container').css({'width' : 'auto'});

  }

}());
